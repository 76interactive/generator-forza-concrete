# Generator Forza Concrete5

## Installation

### 1. Create a directory to store this generator
**For example in your /usr/bin or /httpsdocs**

```
$ mkdir forza-generator && cd forza-generator
```

### 2. Open terminal & install Yeoman

```
$ sudo npm install yo -g
```

### 3. Download this repository to your local disk

```
$ curl -O https://bitbucket.org/76interactive/generator-forza-concrete/get/master.tar.gz
```

### 4. Extract the tarball in the current directory

```
$ tar -xf master.tar.gz -C ./ --strip-components=1
```

### 5. Install this generator

```
$ sudo npm link
```

## Usage

### 1. You'll now be able to use the Yeoman command throughout your system
**Use this on any place of your system you want to generate a new project**

```
$ yo forza-concrete
```

### 2. Install concrete5

```
After your project has been generated, you download the latest stable version of concrete5 (http://www.concrete5.org/download).
Download and open the zip file and copy / paste the concrete folder into your project root directory.
At last, open de application folder inside the zip file and copy / paste the 'files' and 'languages' folders into your project 'application' folder.
```