const Generators = require('yeoman-generator');

const _ = require('lodash');

const Git = require('simple-git');

//const Composer = require('composer');

class AppGenerator extends Generators {
  initializing() {
    this.log(require('yeoman-welcome'));
    this.log('This generator will install your new concrete5 setup and will commit to a git remote of your choice.\n');
  }

  prompting() {
    const prompts = [{
      type: 'input',
      name: 'name',
      message: 'Project name:',
    }, {
      type: 'input',
      name: 'gitRemoteUrl',
      message: 'Git remote url:'
    }];

    return this.prompt(prompts).then((answers) => {
      this.context = Object.assign({}, answers, {
        snakeCaseAppName: _.snakeCase(answers.name),
        camelCaseAppName: _.upperFirst(_.camelCase(answers.name))
      });
    });
  }

  writing() {
    const files = [
      'application',
      '.editorconfig',
      '.babelrc',
      '.htaccess',
      'index.php',
      'README.md',
      'webpack.config.js',
      'package.json'
    ];

    const copyTplWith = (file) => {
      this.fs.copyTpl(
        this.templatePath(file),
        this.destinationPath(file),
        this.context,
        { delimiter: 'FORZA' }
      );
    };

    files.forEach(copyTplWith);

    this.fs.copy(
      this.templatePath('packages/open_graph_tags_lite'),
      this.destinationPath('packages/open_graph_tags_lite')
    );

    this.fs.copy(
      this.templatePath('packages/block_designer'),
      this.destinationPath('packages/block_designer')
    );

    this.fs.copy(
      this.templatePath('packages/block_designer_pro'),
      this.destinationPath('packages/block_designer_pro')
    );

    this.fs.copy(
      this.templatePath('packages/formify'),
      this.destinationPath('packages/formify')
    );

    this.fs.copyTpl(
      this.templatePath('packages/site/controller.php'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/controller.php`),
      this.context,
      { delimiter: 'FORZA' }
    );

    const copyTplThemeDirectory = (directory) => {
      this.fs.copyTpl(
        this.templatePath(`packages/site/${directory}`),
        this.destinationPath(`packages/${this.context.snakeCaseAppName}/${directory}`),
        this.context,
        { delimiter: 'FORZA' }
      );
    };

    copyTplThemeDirectory('elements');
    copyTplThemeDirectory('blocks');
    copyTplThemeDirectory('controllers');
    copyTplThemeDirectory('single_pages');

    this.fs.copyTpl(
      this.templatePath('packages/site/themes/theme'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/themes/${this.context.snakeCaseAppName}`),
      this.context,
      { delimiter: 'FORZA' }
    );

    this.fs.copy(
      this.templatePath('packages/site/themes/theme/assets/images/placeholder.png'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/themes/${this.context.snakeCaseAppName}/assets/images/placeholder.png`)
    );

    this.fs.copy(
      this.templatePath('packages/site/icon.png'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/icon.png`)
    );

    this.fs.copy(
      this.templatePath('packages/site/themes/theme/thumbnail.png'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/themes/${this.context.snakeCaseAppName}/thumbnail.png`)
    );

    this.fs.copy(
      this.templatePath('packages/site/themes/theme/assets/images/logo.png'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/themes/${this.context.snakeCaseAppName}/assets/images/logo.png`)
    );

    this.fs.copy(
      this.templatePath('packages/site/themes/theme/assets/images/logo.svg'),
      this.destinationPath(`packages/${this.context.snakeCaseAppName}/themes/${this.context.snakeCaseAppName}/assets/images/logo.svg`)
    );
  }

  git() {
    this.fs.copy(
      this.templatePath('./.gitignore'),
      this.destinationPath('.gitignore')
    );

    const repository = Git(this.destinationPath('.'));

    return repository.init().exec(() => {
      return repository.addRemote('origin', this.context.gitRemoteUrl);
    }).exec(() => {
      return repository.add(['--all']);
    }).exec(() => {
      return repository.commit('Initial Commit');
    }).exec(() => {
      return repository.push('origin', 'master');
    });
  }

  install() {
    this.installDependencies({ bower: false });
  }
}

module.exports = AppGenerator;
