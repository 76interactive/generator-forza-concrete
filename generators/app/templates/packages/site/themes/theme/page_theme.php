<?php
  namespace Concrete\Package\<FORZA= camelCaseAppName FORZA>\Theme\<FORZA= camelCaseAppName FORZA>;

  use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
  use Concrete\Core\Page\Theme\Theme;
  use Concrete\Core\Asset\Asset;

  defined('C5_EXECUTE') or die("Access Denied.");

  class PageTheme extends Theme {

    public function registerAssets() {
      $currentPage = \Page::getCurrentPage();
      $currentPagePermissions = new \Permissions($currentPage);
      $user = new \User;
      $view = \View::getInstance();

      /**
       * Add files when not loggedin
       */
      if (!$currentPagePermissions->canViewToolbar() || !$user->isLoggedIn()):
        $view->addHeaderItem('<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-9ralMzdK1QYsk4yBY680hmsb4/hJ98xK3w0TIaJ3ll4POWpWUYaA2bRjGGujGT8w" crossorigin="anonymous">');
      endif;
    }

    public function getThemeResponsiveImageMap() {
      return [
        'large' => '1920px',
        'medium' => '640px',
        'small' => '0',
      ];
    }
  }
