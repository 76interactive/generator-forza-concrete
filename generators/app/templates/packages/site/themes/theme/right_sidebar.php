<?php
  View::element('header', '', '<FORZA= snakeCaseAppName FORZA>');
  View::element('hero', '', '<FORZA= snakeCaseAppName FORZA>');
?>

  <section class="section">
    <div class="grid__container">

      <div class="grid__row">
        <div class="grid__col--9 grid__col--sm-12 padding-vertical">
          <?php
            $a = new Area('B1 Row 1 Col 1');
            $a->display($c);
          ?>
          <div class="grid__collapse padding-vertical">
            <div class="grid__col--6 grid__col--sm-12">
              <?php
                $a = new Area('B1 Row 1 Col 2');
                $a->display($c);
              ?>
            </div>
            <div class="grid__col--6 grid__col--sm-12 margin-top__sm">
              <?php
                $a = new Area('B1 Row 1 Col 3');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>
        <aside class="grid__col--3 grid__col--sm-12 padding-vertical">
          <?php
            $a = new Area('Sidebar');
            $a->display($c);
          ?>
        </aside>
      </div>

    </div>
  </section>

<?php View::element('footer', '', '<FORZA= snakeCaseAppName FORZA>'); ?>
