$(() => {
  if ($('.sbs_plp_submitViaAjax').length) {
    $('.sbs_plp_submitViaAjax').each((index, element) => {
      const $PLP = $(element);
      $PLP.find("form").on('submit', (event) => {
        event.preventDefault();
        const $theForm = $(element);
        $theForm.fadeTo(1, .5);
        $PLP.find('.plp_form_loading').show();
        const formValues = $theForm.serialize();
        Concrete.event.publish('page_list_plus/submit', { vals:formValues, form:$theForm });
        return false;
      });
    });

    $('.sbs_plp_receiveViaAjax').on('click', '.pagination a', () => {
      Concrete.event.publish('page_list_plus/paginate', this);
      return false;
    });

    Concrete.event.subscribe("page_list_plus/paginate", (e, el) => {
      const $el = $(el);
      const $PLP = $el.closest('.sbs_plp_receiveViaAjax');
      sbs_plp_ajaxGet($el.attr('href'), $PLP);
    });

    Concrete.event.subscribe("page_list_plus/submit", (e, data) => {
      $('.sbs_plp_receiveViaAjax').each(() => {
        const $PLP = $(this);
        $PLP.attr('data-serialized',data.vals);
        const bID = $PLP.attr('data-bID');
        const cID = $PLP.attr('data-cID');
        const url = $PLP.find('.reloadURL').val()+"?locale="+CCM_ACTIVE_LOCALE+"&bID="+bID+"&cID="+cID+"&ccm_paging_p=1&"+data.vals;

        sbs_plp_ajaxGet(url, $PLP);
      });
    });
  }
});

function sbs_plp_ajaxGet(url, $PLP) {

  const matchHeightElements = [
    '.match--height'
  ];

  $.ajax({
    type: 'GET',
    url: url,
    error: (resp, status, errorthrown) => {
      const $submitViaAjax = $('.sbs_plp_submitViaAjax');
      $submitViaAjax.find('form').fadeTo(.5, 1);
      $submitViaAjax.find('.plp_form_loading').hide();
    },
    success: (resp) => {
      const $resp = $(resp);
      const templateResultAreas = ['.ccm-block-page-list-pages','.pagination'];
      for (const i = 0; i < templateResultAreas.length; i++) {
        const $responseResults = $resp.find(templateResultAreas[i]);
        $PLP.find(templateResultAreas[i]).html($responseResults.children());
      }
      Concrete.event.publish('infinite_scroller/reset', $PLP);
      const $submitViaAjax = $('.sbs_plp_submitViaAjax');
      $submitViaAjax.find('form').fadeTo(.5, 1);
      $submitViaAjax.find('.plp_form_loading').hide();

      $.each(matchHeightElements, (index, item) => {
        $(item).matchHeight({
          remove: true
        });
        $(item).matchHeight();
      });
    }
  });
}

$(document).ready(() => {

  $('.sbs_plp_receiveViaAjax').on('click', '.pagination a', () => {
    $('.sbs_plp_container').find('.plp_form_loading').show();
  });

});
