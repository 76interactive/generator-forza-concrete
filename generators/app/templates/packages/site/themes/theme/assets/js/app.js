import '../scss/style.scss';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
//import 'lity/dist/lity.min.css';

import $ from 'jquery';
import 'jquery-match-height';
import objectFitImages from 'object-fit-images';
import 'owl.carousel';
//import 'velocity-animate/velocity';
//import lity from 'lity/dist/lity.min.js';

import * as TweenMax from "gsap/umd/TweenMax";

import './events.js';
import './page_list_plus.js';

/**
 * Footer always sticks at the bottom of the page
 */

const stickyFooter = () => {
  const footerHeight = $('.footer').outerHeight();
  $('.page__wrap').css({
    'min-height' : $(window).height() - footerHeight
  });
};

$(document).ready(() => {

  objectFitImages();

  $('.match--height').matchHeight();

  $(window).bind('resize', stickyFooter);
  stickyFooter();

});


