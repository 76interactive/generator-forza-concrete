import Vue from 'vue';

/**
 * Responsive side nav event
 */

const headerNavicon = new Vue({
  el: '.header__navicon',
  data: {
    main: $('.header, .page__container, .footer'),
    toggleClass: 'header__navicon--open',
    duration: .5
  },
  methods: {
    toggle: function() {
      const $el = $(this.$el),
      data = this.$data;
      $el.toggleClass(data.toggleClass);
      $el.hasClass(data.toggleClass) ? this.open() : this.close();
    },
    open: function() {
      const data = this.$data;
      TweenMax.to(data.main, data.duration, { x: -250, ease: Circ.easeOut });
    },
    close: function() {
      const $el = $(this.$el),
      data = this.$data;
      $el.removeClass(data.toggleClass);
      TweenMax.to(data.main, data.duration, { x: 0, ease: Circ.easeOut });
    }
  },
});

$(document).ready(() => {

  /**
   * On click window close events
   */

  const windowElements = '.header__navicon, a[href*="#"]';

  $(document).stop().on('click touch touchstart touchend touchmove', window, (e) => {
    $(windowElements).has(e.target).length == 0 && !$(windowElements).is(e.target) ? headerNavicon.close() : '';
  });

});
