<?php
  namespace Concrete\Package\<FORZA= camelCaseAppName FORZA>;

  use Concrete\Core\Package\Package;
  use Concrete\Core\Page\Theme\Theme;
  use Core;
  use SinglePage;
  use Config;
  use Environment;
  use PageType;

  use Concrete\Core\Attribute\Key\CollectionKey as CollectionAttributeKey;
  use Concrete\Core\Attribute\Key\Category as AttributeKeyCategory;
  use Concrete\Core\Attribute\Type as AttributeType;
  use Concrete\Core\Attribute\Set as AttributeSet;
  use Concrete\Core\Page\Type\Composer\Control\CollectionAttributeControl as AttributeControl;

  defined('C5_EXECUTE') or die(_("Access Denied."));

  class Controller extends Package {
    protected $pkgHandle  = '<FORZA= snakeCaseAppName FORZA>';
    protected $pkgVersion = '1.0.0';

    protected $appVersionRequired = '8.0.0';

    public function getPackageDescription() {
      return t('Adds the <FORZA= camelCaseAppName FORZA> theme.');
    }

    public function getPackageName(){
      return t('<FORZA= camelCaseAppName FORZA>');
    }

    public function install() {
      $pkg = parent::install();
      Theme::add('<FORZA= snakeCaseAppName FORZA>', $pkg);
      $this->config();
    }

    public function upgrade() {
      parent::upgrade();
      $this->config();
      // $sp = SinglePage::add('/contact/bedankt', $this);
      // if (is_object($sp)) {
      //   $sp->update(array(
      //     'cName' => t('Bedankt'),
      //     'cDescription' => t('')
      //   ));
      // }
    }

    public function on_start() {
      $environment = Environment::get();
    }

    public function config() {
      $pkgHandle = $this->getPackageHandle();
      Config::save('concrete.white_label.logo', Core::getApplicationURL() . '/packages/' . $pkgHandle . '/icon.png');
      Config::save('concrete.urls.background_url', Core::getApplicationURL() . '/packages/' . $pkgHandle . '/wallpaper.png');
      $this->installPackageAttributes();
    }

    public function installPackageAttributes() {
      $eaku = AttributeKeyCategory::getByHandle('collection');
      $eaku->setAllowAttributeSets(AttributeKeyCategory::ASET_ALLOW_SINGLE);
      if (!AttributeSet::getByHandle('page')):
        $set = $eaku->addSet('page', t('Pagina'), $pkg, 0);
        $asID = $set->getAttributeSetID();
      else:
        $set = AttributeSet::getByHandle('page');
        $asID = $set->getAttributeSetID();
      endif;

      // Create thumbnail attribute under attribute set
      if (!$key || !intval($key->getAttributeKeyID())):
        $attr_type = AttributeType::getByHandle('image_file');
        $desc = [
          'akHandle' => 'thumbnail',
          'akName'=> t('Thumbnail'),
          'asID' => $asID,
          'akIsSearchable' => false
        ];
        $key = CollectionAttributeKey::add($attr_type, $desc, $pkg);
      endif;

      // Assign attributes to page type
      $pt = PageType::getByHandle('page');
      $pImage = CollectionAttributeKey::getByHandle('thumbnail')->getAttributeKeyID();

      $pAttributeSet = $pt->addPageTypeComposerFormLayoutSet('Custom attributes', '');

      if ($pAttributeSet):
        $pAttributeImage = new AttributeControl();
        $pAttributeImage->setAttributeKeyID($pImage);
        $pAttributeImage->addToPageTypeComposerFormLayoutSet($pAttributeSet);
      endif;
    }
  }
