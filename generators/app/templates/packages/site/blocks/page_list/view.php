<?php
  defined('C5_EXECUTE') or die("Access Denied.");

  $c = Page::getCurrentPage();

  /** @var \Concrete\Core\Utility\Service\Text $th */
  $th = Core::make('helper/text');
  /** @var \Concrete\Core\Localization\Service\Date $dh */
  $dh = Core::make('helper/date');
?>
<?php if ( $c->isEditMode() && $controller->isBlockEmpty()): ?>
<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php else: ?>
<ul>
  <?php
    foreach ($pages as $page):
      $title = $page->getCollectionName();
      if ($page->getCollectionPointerExternalLink() != ''):
        $url = $page->getCollectionPointerExternalLink();
        if ($page->openCollectionPointerExternalLinkInNewWindow()):
          $target = '_blank';
        endif;
      else:
        $url = $page->getCollectionLink();
        $target = $page->getAttribute('nav_target');
      endif;
      $target = empty($target) ? '_self' : $target;
      $description = $page->getCollectionDescription();
      $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;

      $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);

      $thumbnail = $page->getAttribute('thumbnail');
      if (is_object($thumbnail)):
        $img = Core::make('html/image', array($thumbnail));
        $tag = $img->getTag();
        $tag->addClass('image--cover');
      endif;
  ?>

  <li><a href="<?php echo $url; ?>"><?php echo $title; ?></a></li>

<?php endforeach; ?>
</ul>
<?php if (count($pages) == 0): ?>
<div class="ccm-block-page-list-no-pages"><?php echo h($noResultsMessage); ?></div>
<?php endif; ?>

<?php
  if ($showPagination):
    echo $pagination;
  endif;
?>

<?php endif; ?>
