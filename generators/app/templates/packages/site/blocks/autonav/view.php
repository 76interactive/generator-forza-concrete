<?php
  defined('C5_EXECUTE') or die("Access Denied.");

  $navItems = $controller->getNavItems();
  $c = Page::getCurrentPage();

  foreach ($navItems as $ni):
    $classes = array();

    if ($ni->isCurrent || $ni->inPath):
      $classes[] = 'header__item--active';
    endif;

    $ni->classes = implode(" ", $classes);
  endforeach;

  if (count($navItems) > 0):
    echo '<ul class="header__list list-style--none">';
    foreach ($navItems as $ni):
      if ($ni->level == 1):
        echo '<li class="header__item ' . $ni->classes . '">';
      else:
        echo '<li class="header__subitem ' . $ni->classes . '">';
      endif;

      echo '<a href="' . $ni->url . '" target="' . $ni->target . '" class="' . $ni->classes . '">' . $ni->name . '</a>';

      if ($ni->hasSubmenu):
        echo '<ul class="header__sublist">';
      else:
        echo '</li>';
        echo str_repeat('</ul></li>', $ni->subDepth);
      endif;
    endforeach;
    echo '</ul>';
  elseif (is_object($c) && $c->isEditMode()):
?>
  <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Auto-Nav Block.') ?></div>
<?php endif; ?>
