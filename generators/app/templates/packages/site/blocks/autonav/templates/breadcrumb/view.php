<?php defined('C5_EXECUTE') or die("Access Denied.");

$navItems = $controller->getNavItems(true); // Ignore exclude from nav
$c = Page::getCurrentPage();

$i = 1;

if (count($navItems) > 0) {
  echo '<ol class="breadcrumb__list" itemscope itemtype="http://schema.org/BreadcrumbList">';
  foreach ($navItems as $ni) {
    if ($ni->isCurrent) {
      echo '<li class="breadcrumb__item breadcrumb__item--active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . $ni->name . '</span><meta itemprop="position" content="' . $i++ . '" /></li>';
    } else {
      echo '<li class="breadcrumb__item" itemprop="itemListElement" itemscope
itemtype="http://schema.org/ListItem"><a itemscope itemtype="http://schema.org/Thing"
itemprop="item" href="' . $ni->url . '" target="' . $ni->target . '"><span itemprop="name">' . $ni->name . '</span></a><i class="fa fa-angle-right"></i><meta itemprop="position" content="' . $i++ . '" /></li>';
    }
  }
  echo '</ol>';
} elseif (is_object($c) && $c->isEditMode()) { ?>
  <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Auto-Nav Block.'); ?></div>
<?php } ?>
