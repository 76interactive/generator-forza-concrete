<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<ul class="social__list">
<?php
  foreach($links as $link) {
    $service = $link->getServiceObject();
?>
  <li class="social__item"><a target="_blank" href="<?php echo h($link->getURL()); ?>"><?php echo $service->getServiceIconHTML(); ?></a></li>
  <?php } ?>
</ul>

