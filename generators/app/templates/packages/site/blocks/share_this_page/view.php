<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<ul class="social__list">
<?php foreach($selected as $service) { ?>
  <li class="social__item float--left"><a class="text-decoration--none color--black" target="_blank" href="<?php echo h($service->getServiceLink()); ?>" aria-label="<?php echo h($service->getDisplayName()) ?>"><?php echo $service->getServiceIconHTML(); ?></a></li>
<?php } ?>
</ul>

