<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $c = Page::getCurrentPage();
?>
<!doctype html>
<html lang="nl">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <?php View::element('header_required'); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath(); ?>/dist/css/app.css" />
</head>
<body>

  <div class="page width--stretch height--stretch float--left relative"><!-- Start Page -->

    <div class="page__wrap width--stretch float--left relative"><!-- Start Page Wrap -->

      <!-- Header -->
      <header class="header width--stretch float--left fixed background--white">
        <div class="grid__container height--stretch">
          <div class="grid__row height--stretch">
            <div class="grid__col--12 height--stretch">
              <div class="table width--stretch">
                <div class="table__cell table__cell--vertical-middle">
                <a class="header__logo float--left" title="<FORZA= camelCaseAppName FORZA>" href="<?php echo View::url('/'); ?>"><img class="image--contain" src="<?php echo $view->getThemePath() ?>/assets/images/logo.svg" alt="<FORZA= camelCaseAppName FORZA>" /></a>
                  <nav class="header__nav float--right">
                    <?php
                      $a = new GlobalArea('Header Navigation');
                      $a->display($c);
                    ?>
                  </nav>
                  <a class="header__navicon float--right margin-top--small" v-on:click="toggle()">
                    <div class="header__navicon--bars"></div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div class="page__container width--stretch float--left relative"><!-- Start Page Container -->
