<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

      </div><!-- End Page Container -->

    </div><!-- End Page Wrap -->

    <footer class="footer width--stretch float--left relative"">
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--12 padding-vertical">
            <p>&copy; Copyright <?php echo date('Y') . ' <FORZA= camelCaseAppName FORZA>'; ?></p>
          </div>
        </div>
      </div>
    </footer>

  </div><!-- End Page -->

  <?php View::element('footer_required'); ?>
  <script async defer src="<?php echo $view->getThemePath(); ?>/dist/js/app.js"></script>
</body>
</html>


