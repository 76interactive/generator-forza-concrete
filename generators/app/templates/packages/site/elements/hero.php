<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $c = Page::getCurrentPage();

  $thumbnail = $c->getAttribute('thumbnail');
  if (is_object($thumbnail)):
    $img = Core::make('html/image', [($thumbnail)]);
    $tag = $img->getTag();
    $tag->addClass('background--image');
  endif;
?>

  <section class="hero width--stretch height--medium float--left relative">
    <?php echo ($thumbnail ? $tag : ''); ?>
    <div class="hero__overlay width--stretch height--stretch float--left absolute background--black background--transparent background--light"></div>
    <div class="grid__container height--stretch">
      <div class="grid__row height--stretch">
        <div class="grid__col--12 height--stretch padding-vertical--large color--white">
          <div class="table width--stretch">
            <div class="table__cell table__cell--vertical-middle">
              <?php
                $a = new Area('Hero');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
