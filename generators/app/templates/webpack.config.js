'use strict';

const path = require('path');
const webpack = require('webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const themeDirectory = '/packages/<FORZA= snakeCaseAppName FORZA>/themes/<FORZA= snakeCaseAppName FORZA>';

var config = {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/,
        options: {
          babelrc: true,
          cacheDirectory: true,
          presets: [
            [
              '@babel/preset-env', {
                useBuiltIns: 'entry',
                targets: {
                  esmodules: true
                }
              },
              '@babel/react'
            ]
          ],
          plugins: [
            '@babel/proposal-class-properties',
            '@babel/plugin-transform-block-scoping'
          ]
        },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /fonts\/[^.]+\.(ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?name=/fonts/[hash].[ext]',
      },
      {
        test: /\.(svg|png|jpg|jpeg|gif)$/,
        loader: 'file-loader?name=/images/[hash].[ext]',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new BrowserSyncPlugin({
      proxy: 'http://<FORZA= snakeCaseAppName FORZA>.enorm'
    }),
    new MiniCssExtractPlugin({
      filename: "/css/[name].css",
      chunkFilename: "/css/[id].css"
    }),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.optimize\.css$/g,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: { discardComments: { removeAll: true } },
      canPrint: true
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ],
  stats: {
    children: false,
    modules: false,
  },
}

var appConfig = Object.assign({}, config, {
  entry: {
    app: ['@babel/polyfill', path.join(__dirname, themeDirectory, '/assets/js/app.js')],
  },
  output: {
    path: path.join(__dirname, themeDirectory, 'dist'),
    filename: './js/[name].js',
    publicPath: path.join(
      '/',
      themeDirectory,
      'dist'
    ),
  },
});

module.exports = (env, argv) => {
  switch(argv.mode) {
    case 'development':
      appConfig.devtool = 'eval-source-map';
      break;
    case 'production':
      appConfig.devtool = 'none';
      break;
  }
  return appConfig;
};
