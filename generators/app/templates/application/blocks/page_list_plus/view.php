<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $c = Page::getCurrentPage();
  $dateHelper = Core::make('helper/date');
  /* @var $dateHelper \Concrete\Core\Localization\Service\Date */
  $navigationHelper = Core::make('helper/navigation');
  $textHelper = Core::make('helper/text');

  $wrapperClasses = ['ccm-block-page-list-wrapper', 'sbs_plp_container'];
  if ($controller->allowPagination) $wrapperClasses[] = 'sbs_plp_hasPagination';
  if (($controller->showSearchFilters || $controller->showSearchBox) && $controller->submitViaAjax) $wrapperClasses[] = 'sbs_plp_submitViaAjax';
  if ($controller->showSearchResults && $controller->receiveViaAjax) $wrapperClasses[] = 'sbs_plp_receiveViaAjax';
  $wrapperClasses = implode(' ', $wrapperClasses);
?>

<div class="<?php echo $wrapperClasses; ?>" id="sbs_plp_container-<?php echo $controller->getIdentifier()?>" data-bID="<?php echo $controller->getIdentifier(); ?>" data-cID="<?php echo $c->cID; ?>" style="position: relative;">
  <input type="hidden" class="reloadURL" value="<?php echo URL::route(array('/reload', 'skybluesofa_page_list_plus')); ?>">
  <?php
    echo Loader::element('title', ['controller' => $controller], 'skybluesofa_page_list_plus');
    echo Loader::element('form', ['controller' => $controller], 'skybluesofa_page_list_plus');
    echo Loader::element('rss', ['controller' => $controller], 'skybluesofa_page_list_plus');
  ?>
  <?php if ($c->isEditMode() && $controller->isBlockEmpty()): ?>
  <div class="ccm-edit-mode-disabled-item">
    <?php echo t('Empty Page List+ Block.') ?>
  </div>
  <?php else: ?>
  <ul class="ccm-block-page-list-pages">
  <?php
    $i = 0;
    foreach ($controller->pages as $page):
      $title = $textHelper->entities($page->getCollectionName());

      $url = $navigationHelper->getLinkToCollection($page);
      $target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
      $target = empty($target) ? '_self' : $target;

      $description = $page->getCollectionDescription();
      $description = $controller->truncateSummaries ? $textHelper->wordSafeShortText($description, $controller->truncateChars) : $description;
      $description = $textHelper->entities($description);

      $thumbnail = false;
      if ($controller->includeThumbnail):
        $thumbnail = $controller->getThumbnailAttribute($page);
        if (is_object($thumbnail)):
          $img = Core::make('html/image', [$thumbnail]);
          $tag = $img->getTag();
          $tag->addClass('image--cover');
        endif;
      endif;

      $date = $dateHelper->formatDateTime($page->getCollectionDatePublic(), true);

      /*
       * $controller->includeName
       * $controller->includeDate
       * $controller->includeDescription
       */
  ?>

    <li><a href="<?php echo $url; ?>"><?php echo $title; ?></a></li>

  <?php endforeach; ?>

  <?php if (count($controller->pages) == 0): ?>
    <div class="ccm-block-page-list-no-pages"><?php echo $noResultsText ?></div>
  <?php endif; ?>
  <?php echo Loader::element('pagination', ['controller' => $controller], 'skybluesofa_page_list_plus'); ?>
  <?php endif; ?>
  </ul>
</div>
