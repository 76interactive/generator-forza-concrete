<?php
  namespace Application\Block\PageListPlus;

  use Concrete\Core\Page\Page;
  use Concrete\Package\SkybluesofaPageListPlus\PageListPlus\PageListPlus;

  defined('C5_EXECUTE') or die("Access Denied.");

  class Controller extends \Concrete\Package\SkybluesofaPageListPlus\Block\PageListPlus\Controller {

    public function on_start() {
      $cObj = Page::getCurrentPage();
      $this->addFooterItem('<script>var REL_DIR_FILES_TOOLS_PACKAGES="' . REL_DIR_FILES_TOOLS_PACKAGES . '";var sbs_cID="' . $cObj->cID . '";</script>', 'SCRIPT');
      $al = \Concrete\Core\Asset\AssetList::getInstance();
      $al->registerGroup('skybluesofa/pagelistplus', [
        ['javascript', 'jquery'],
        ['javascript', 'core/events'],
        ['javascript', 'underscore'],
      ]);
    }

    public function setupFormElements() {
      parent::setupFormElements();
      $this->showSearchSelectAsCheckboxAttributes  = (!is_array($this->showSearchSelectAsCheckboxAttributes) ? explode(',', $this->showSearchSelectAsCheckboxAttributes) : $this->showSearchSelectAsCheckboxAttributes);
      $this->pageAttributeIdsUsedInSearch = (!is_array($this->pageAttributeIdsUsedInSearch) ? explode(',', $this->pageAttributeIdsUsedInSearch) : $this->pageAttributeIdsUsedInSearch);
    }
  }
