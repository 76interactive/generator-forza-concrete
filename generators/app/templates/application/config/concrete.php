<?php

return [
    'marketplace' => [
        'enabled' => false
    ],
    'external' => [
        'news_overlay' => false,
        'news' => false,
    ],
    'session' => [
        'save_path' => '/tmp',
    ],
];
