<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php if ($controller->submitOnChangeOfFilter || empty($controller->searchBoxButtonText)): ?>
<script type="text/javascript">

  var typingTimer;
  var doneTypingInterval = 500;
  var $sbs_plp_submitEvents_<?php echo $controller->getIdentifier(); ?> = [];

  $(function () {
    var $sbs_plp_form_<?php echo $controller->getIdentifier(); ?> = $('#sbs_plp_form_<?php echo $controller->getIdentifier(); ?>');

    function doneTyping() {
      var doSubmit = true;
      for (var eventId = 0; eventId < $sbs_plp_submitEvents_<?php echo $controller->getIdentifier(); ?>.length; eventId++) {
        var shouldSubmit = $sbs_plp_submitEvents_<?php echo $controller->getIdentifier(); ?>[eventId]();
        if (!shouldSubmit) {
          return false;
        }
      }
      if (doSubmit) {
        var $theForm = $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>;
        $('.sbs_plp_submitViaAjax').find('.plp_form_loading').show();
        var formValues = $theForm.serialize();
        Concrete.event.publish('page_list_plus/submit', {vals:formValues, form:$theForm});
      }
    }

    $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.find(':input').not('.sbs_plp_query').bind("keypress", function (event) {
      if (event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });

    $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.find(':input').not(':checkbox').focusout(function () {
      clearTimeout(typingTimer);
      $(this).css({
        'opacity':'1'
      });
    });

    $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.find(':input').not(':checkbox').keyup(function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.find(':input').not(':checkbox').on('keydown', function () {
      clearTimeout(typingTimer);
    });

    $('#sbs_plp_form_<?php echo $controller->getIdentifier(); ?> button[type=submit]').bind('click', function () {
      var doSubmit = true;
      for (var eventId = 0; eventId < $sbs_plp_submitEvents_<?php echo $controller->getIdentifier(); ?>.length; eventId++) {
        var shouldSubmit = $sbs_plp_submitEvents_<?php echo $controller->getIdentifier(); ?>[eventId]();
        if (!shouldSubmit) {
          return false;
        }
      }
      if (doSubmit) {
        $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.trigger('submit');
      }
    });

    $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.find(':checkbox').bind('click', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    $('#sbs_plp_form_<?php echo $controller->getIdentifier(); ?> button[type=reset]').bind('click', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
      $sbs_plp_form_<?php echo $controller->getIdentifier(); ?>.find('.formify-checkbox-label').removeClass('formify-checked');
    });

  });
</script>
<?php endif; ?>
