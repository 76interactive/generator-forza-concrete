<?php
  defined('C5_EXECUTE') or die("Access Denied.");

  use Concrete\Attribute\Topics\Controller as TopicsAttributeTypeController;
  use Concrete\Core\Attribute\Type as AttributeType;
  use \Concrete\Core\Tree\Type\Topic as TopicTree;
  use Concrete\Core\Tree\Node\Node as TreeNode;

  use Concrete\Package\SkybluesofaPageListPlus\PageListPlus\PageListPlus;

  $showAsDropdown = false;
  if (isset($controller->pageAttributesUsedForFilter[$filter->getAttributeKeyID()])):
    if (in_array($controller->pageAttributesUsedForFilter[$filter->getAttributeKeyID()]['filterSelection'], ['querystring_all', 'not_querystring_all'])):
      $showAsDropdown = true;
    endif;
  endif;

  $usedValues = array_reduce($controller->totalPages, function($carry, $item) use ($filter) {
    $carry[] = (string) ($item->getAttribute($filter->getAttributeKeyHandle(), 'display'));
    return $carry;
  }, []);

  array_filter($usedValues);
  $usedValues = array_unique($usedValues);

  if ($showAsDropdown):
    $options = [];
    $ak = CollectionAttributeKey::getByHandle($filter->getAttributeKeyHandle());
    $cnt = $ak->getController();
    $treeId = $cnt->getTopicTreeID();
    $tree = TopicTree::getByID($treeId);

    $root = $tree->getRootTreeNodeObject();
    $childNodeIds = $root->getAllChildNodeIDs();

    $nodePaths = [];
    foreach ($childNodeIds as $childNodeId):
      $node = TreeNode::getByID($childNodeId);
      $nodePath = substr($node->getTreeNodeDisplayPath('text'), 1);
      if ($node->getTreeNodeTypeHandle() == 'topic'):
        $options[$nodePath] = $node->getTreeNodeDisplayName();
      endif;
    endforeach;

    if ($controller->alphabetizeSearchSelects):
      natcasesort($options);
    endif;

    $options = array_unique($options);

    $getValue = [];
    if (isset($controller->searchDefaults[$filter->getAttributeKeyID()]) && !empty($controller->searchDefaults[$filter->getAttributeKeyID()])):
      $getValue[] = strtolower($controller->searchDefaults[$filter->getAttributeKeyID()]);
    endif;
    if (isset($_GET[$filter->getAttributeKeyHandle()])):
      $getValue = [];
      if (is_array($_GET[$filter->getAttributeKeyHandle()])):
        foreach ($_GET[$filter->getAttributeKeyHandle()] as $get):
          $getValue[] = strtolower($get);
        endforeach;
      else:
        $getValue[] = strtolower($_GET[$filter->getAttributeKeyHandle()]);
      endif;
    endif;
    $showSearchSelectAsCheckboxAttributes = $controller->showSearchSelectAsCheckboxAttributes;
?>

  <?php if (!$controller->showSearchSelectsAsCheckbox || ($controller->showSearchSelectsAsCheckbox && !in_array($filter->getAttributeKeyID(), (array) $showSearchSelectAsCheckboxAttributes))): ?>
  <select
    name="<?php echo $filter->getAttributeKeyHandle(); ?>"
    class="plp_<?php echo $filter->getAttributeKeyHandle(); ?>"
    placeholder="<?php echo tc('AttributeKeyName', $filter->getAttributeKeyName()); ?>">
    <?php
      $options = array_reverse($options, true);
      $options[''] = $controller->nameAsSearchFilterAllText ? $filter->getAttributeKeyName() : t($controller->searchFilterAllText);
      $options = array_reverse($options, true);

      foreach ($options as $value => $title):
        if (in_array($title, $usedValues)):
    ?>
    <option value="<?php echo $value; ?>" <?php echo (in_array(strtolower($value), $getValue) ? 'selected="selected"' : ''); ?>>
      <?php echo tc('TreeNodeCategoryName', $title) . sprintf(' (%d)', array_count_values($usedValues)[$title]); ?>
    </option>
    <?php
        endif;
      endforeach;
    ?>
  </select>
  <?php else: ?>
  <div class="sbs_plp_selectboxReplacement" id="sbs_plp_selectboxReplacement_<?php echo $filter->getAttributeKeyHandle(); ?>">
    <div class="formify-field-input">
      <fieldset class="formify-fieldset">
        <?php
          $i = 0;
          foreach ($options as $value => $title):
            if (in_array($title, $usedValues)):
        ?>
        <label class="formify-checkbox-label">
          <input class="formify-field formify-checkbox plp_<?php echo $filter->getAttributeTypeHandle(); ?>" type="checkbox" name="<?php echo $filter->getAttributeKeyHandle(); ?>[<?php echo $i; ?>]" value="<?php echo $value; ?>" <?php echo (in_array(strtolower($value), $getValue) ? 'checked="checked"' : ''); ?>>
          <i class="formify-check "></i>
          <span><?php echo $title; ?></span>
        </label>
        <?php
            $i++;
            endif;
          endforeach;
        ?>
      </fieldset>
    </div>
  </div>
  <?php endif; ?>

<?php endif; ?>
