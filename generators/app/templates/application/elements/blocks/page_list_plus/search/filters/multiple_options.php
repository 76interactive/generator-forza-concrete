<?php
  defined('C5_EXECUTE') or die("Access Denied.");

  use Concrete\Attribute\Select\Controller as SelectAttributeTypeController;
  use Concrete\Core\Attribute\Type as AttributeType;

  $options = [];
  if ($filter->getAttributeTypeHandle() == "select"):
    Loader::model('attribute/type');
    $satc = Core::make('Concrete\Attribute\Select\Controller');
    $satc->setAttributeKey(CollectionAttributeKey::getByHandle($filter->getAttributeKeyHandle()));
    $values = $satc->getOptions();
    foreach ($values as $v):
      $optionValue = $v->getSelectAttributeOptionValue();
      trim($optionValue);
      if (strlen($optionValue)):
        $options[$optionValue] = $optionValue;
      endif;
    endforeach;
    if ($controller->alphabetizeSearchSelects):
      natcasesort($options);
    endif;
  else:
    $options['false'] = 'False';
    $options['true'] = 'True';
  endif;
  $getValue = [];
  if (isset($controller->searchDefaults[$filter->getAttributeKeyID()]) && !empty($controller->searchDefaults[$filter->getAttributeKeyID()])):
    $getValue[] = $controller->searchDefaults[$filter->getAttributeKeyID()];
  endif;
  $request = Request::getInstance();
  if (isset($_GET[$filter->getAttributeKeyHandle()])):
    $getValue = [];
    if (is_array($_GET[$filter->getAttributeKeyHandle()])):
      foreach ($_GET[$filter->getAttributeKeyHandle()] as $get):
        $getValue[] = strtolower($get);
      endforeach;
    else:
      $getValue[] = strtolower($_GET[$filter->getAttributeKeyHandle()]);
    endif;
  endif;
  $showSearchSelectAsCheckboxAttributes = $controller->showSearchSelectAsCheckboxAttributes;
?>

<?php
  if (!$controller->showSearchSelectsAsCheckbox || ($controller->showSearchSelectsAsCheckbox && !in_array($filter->getAttributeKeyID(), $showSearchSelectAsCheckboxAttributes))):
    $options = array_reverse($options, true);
    $options[''] = $controller->nameAsSearchFilterAllText ? $filter->getAttributeKeyName() : t($controller->searchFilterAllText);
    $options = array_reverse($options, true);
?>
  <label><?php echo tc('AttributeKeyName', $filter->getAttributeKeyName()); ?></label>
  <select
    name="<?php echo $filter->getAttributeKeyHandle(); ?>"
    class="plp_<?php echo $filter->getAttributeKeyHandle(); ?>">
    <?php foreach ($options as $value => $title): ?>
    <option value="<?php echo $value; ?>" <?php echo (in_array($value, $getValue) ? 'selected="selected"' : ''); ?>>
      <?php echo tc('SelectAttributeValue', $title); ?>
    </option>
    <?php endforeach; ?>
  </select>
<?php else: ?>
  <div class="sbs_plp_selectboxReplacement" id="sbs_plp_selectboxReplacement_<?php echo $filter->getAttributeKeyHandle(); ?>">
    <div class="formify-field-input">
      <fieldset class="formify-fieldset">
        <?php
          $i = 0;
          foreach ($options as $value => $title):
            if (in_array($title, $usedValues)):
        ?>
        <label class="formify-checkbox-label">
          <input class="formify-field formify-checkbox plp_<?php echo $filter->getAttributeTypeHandle(); ?>" type="checkbox" name="<?php echo $filter->getAttributeKeyHandle(); ?>[<?php echo $i; ?>]" value="<?php echo $value; ?>" <?php echo (in_array(strtolower($value), $getValue) ? 'checked="checked"' : ''); ?>>
          <i class="formify-check"></i>
          <span><?php echo $title; ?></span>
        </label>
        <?php
            $i++;
            endif;
          endforeach;
        ?>
      </fieldset>
    </div>
  </div>
<?php endif; ?>
